package httpserver

import "github.com/sirupsen/logrus"

type HTTPServer struct {
	config *Config
	logger *logrus.Logger
}

func NewServer(config *Config) *HTTPServer {
	return &HTTPServer{
		config: config,
		logger: logrus.New(),
	}
}

func (s *HTTPServer) configLogger() error {
	levl, err := logrus.ParseLevel(s.config.logLevel)
	if err != nil {
		return err
	}
	s.logger.SetLevel(levl)
	return nil
}

func (s *HTTPServer) Start() error {
	if err := s.configLogger(); err != nil {
		return err
	}
	s.logger.Info("Starting HTTP server")
	return nil
}
