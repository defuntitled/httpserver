package httpserver

import (
	"github.com/BurntSushi/toml"
)

type Config struct {
	Addres   string `toml :"addres"`
	logLevel string `toml: "log_level"`
}

func (c *Config) configFromToml(config_path string) error {
	_, err := toml.DecodeFile(config_path, c)
	if err != nil {
		return err
	}
	return nil
}

func (c *Config) NewConfig() *Config {
	return &Config{
		Addres:   ":8080",
		logLevel: "debug",
	}
}
